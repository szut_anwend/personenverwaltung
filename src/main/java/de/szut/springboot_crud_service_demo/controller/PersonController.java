package de.szut.springboot_crud_service_demo.controller;

import de.szut.springboot_crud_service_demo.model.Person;
import de.szut.springboot_crud_service_demo.repository.PersonRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Optional;

@RestController
public class PersonController {
    @Autowired
    private PersonRepository repository;

    @RequestMapping(value = "/SpringBootCrudService/person",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public Person createPerson(@RequestBody Person newPerson) {
        return this.repository.save(newPerson);
    }

    @RequestMapping(value = "/SpringBootCrudService/person/{id}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public Person getPersonById(@PathVariable long id) {
        Optional<Person> person = this.repository.findById(id);
        if(person.isPresent())
            return person.get();
        return null;
    }

    @RequestMapping(value = "/SpringBootCrudService/person",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Person> getAllPersons() {
        return this.repository.findAll();
    }

    @RequestMapping(value = "/SpringBootCrudService/person",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public Person updatePerson(@RequestBody Person personToUpdate) {
        Optional<Person> personOptional= this.repository.findById(personToUpdate.getId());
        if(personOptional.isPresent()){
            Person person = personOptional.get();
            person.setFirstname(personToUpdate.getFirstname());
            person.setSurname(personToUpdate.getSurname());
            return this.repository.save(person);
        }
        return null;
    }

    @RequestMapping(value = "/SpringBootCrudService/person/{id}",
            method = RequestMethod.DELETE)
    public void deletePersonById(@PathVariable("id") long id) {
        this.repository.deleteById(id);
    }

    @RequestMapping(value = "/SpringBootCrudService/person/welcome",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String welcome() {
        return "Willkommen in der API zur Verwaltung von Personen!";
    }
}
